import schedule
import time
from hPoloniexRequest import polo
from hbuysell import data
from hfirebase import firebase
import pandas as pd


class preparingdata:
    def currentdata(self, pbch):
        pushdata = {}
        pushdata["last"] = pbch["last"]
        pushdata["low"] = pbch["low24hr"]
        pushdata["high"] = pbch["high24hr"]
        pushdata["percent"] = pbch["percentChange"]
        pushdata["lowestAsk"] = pbch["lowestAsk"]
        pushdata["highestBid"] = pbch["highestBid"]
        pushdata["baseVol"] = pbch["baseVolume"]
        pushdata["quoteVol"] = pbch["quoteVolume"]
        return (pushdata)

    def OT_DataCalc(self, data):
        # data=json.load(open('text.json'))
        ask = pd.DataFrame(data["asks"], columns=['a', 'b'])

        ask['c'] = pd.to_numeric(ask['a']) * pd.to_numeric(ask['b'])
        askPrice = ask['c'].sum() / pd.to_numeric(ask['b']).sum()

        buy = pd.DataFrame(data["bids"], columns=['a', 'b'])
        buy['c'] = pd.to_numeric(buy['a']) * pd.to_numeric(buy['b'])
        buyPrice = buy['c'].sum() / pd.to_numeric(buy['b']).sum()
        print({  'buy' : buyPrice  , 'Ask' : askPrice } )
        return (buyPrice, askPrice)
        
        
    def ordersData(self,data):
        data=pd.DataFrame(data['asks'],columns=['price','quant'])
        
        print(data)
        m=1
        

    def BCHdata(self):
        pbch = polo().returnLastPrice("BTC_BCH")
        pushdata = self.currentdata(pbch)
             
        bch_25 = polo().returnOrderBook("BTC_BCH", 25)
        pushdata["OT_Buy25"] , pushdata["OT_Ask25"] = self.OT_DataCalc(bch_25)
        bch_50 = polo().returnOrderBook("BTC_BCH", 50)
        pushdata["OT_Buy50"] , pushdata["OT_Ask50"] = self.OT_DataCalc(bch_50)
        bch_75 = polo().returnOrderBook("BTC_BCH", 75)
        pushdata["OT_Buy75"] , pushdata["OT_Ask75"] = self.OT_DataCalc(bch_75)
        bch_100 = polo().returnOrderBook("BTC_BCH", 100)
        pushdata["OT_Buy100"] , pushdata["OT_Ask100"] = self.OT_DataCalc(bch_100)
        bch_150 = polo().returnOrderBook("BTC_BCH", 150)
        pushdata["OT_Buy150"] , pushdata["OT_Ask150"] = self.OT_DataCalc(bch_150)
        bch_200 = polo().returnOrderBook("BTC_BCH", 200)
        pushdata["OT_Buy200"] , pushdata["OT_Ask200"] = self.OT_DataCalc(bch_200)
        
        
        
        ordersdata = polo().returnOrderBook("BTC_BCH", 80)
        self.ordersData(ordersdata)

        
        
        pushdata["bchtradebuyprice"] = bchtradebuyprice
        pushdata["bchtradesellprice"] = bchtradesellprice
        firebase().pushBCHdata(pushdata)
        print("fine")


print(preparingdata().BCHdata())


#def job():
#preparingdata().BCHdata()



#schedule.every(30).minutes.do(job)
#while True:
#    schedule.run_pending()
#    time.sleep(1)
