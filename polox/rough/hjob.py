import schedule
import time
from hPoloniexRequest import polo
from hbuysell import data


def job():
    pTic = polo().returnLastPrice("BTC_BCH")
    vlast = float(pTic['last'])
    vhigh = float(pTic['high24hr'])
    vlow = float(pTic['low24hr'])
    xfac = (vhigh - vlow)

    b = 6
    a = 5

    if (vlow + (xfac / b) > vlast):
        data().tobuy("b", b)

    if (vlow + (xfac / a) > vlast):
        data().tobuy("a", a)

    data().tosell("a", a)
    data().tosell("b", b)


schedule.every(1).minutes.do(job)
while True:
    schedule.run_pending()
    time.sleep(1)
