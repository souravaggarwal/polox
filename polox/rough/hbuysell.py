from hfirebase import firebase
from hPoloniexRequest import polo


class data:
    # for Buying
    def checkfirebase(self, child):
        user = firebase().Getbuying(child, "bUseractive")
        isbuying = firebase().Getbuying(child, "bIsbuying")
        quant = firebase().Getbuying(child, "bQuantity")
        quantity = float("{:10.7f}".format(float(quant)))
        # print(user,isbuying,quantity)
        return ((int(user) and int(isbuying)), quantity)

    def updateSellingDB(self, child, price, bchTradeAmt):
        sellingPrice = str(round(float(price) * 1.04, 7))
        bchbalance = str(round(float(bchTradeAmt) * 0.9975, 7))
        data = {"BCHBalance": bchbalance, "SellingPrice": sellingPrice}
        firebase().Setselling(child, data)
        print("Updating SellingDB BalPr ", bchbalance, sellingPrice)

    def tobuy(self, child, factor):
        pushdata = {"Type": "ToBuy", "Category": str(child) + str(factor)}
        resultData = 0
        try:

            # check according to current price it is buyable ?
            pTic = polo().returnLastPrice("BTC_BCH")
            vlast = float(pTic['last'])
            vhigh = float(pTic['high24hr'])
            vlow = float(pTic['low24hr'])

            xf = (vhigh - vlow)
            vl = round(vlow + (((vhigh - vlow) / 4) * 3), 7)

            bestprice = round(float(vlow + (xf / factor)), 7)
            boundryprice = round(float(vl * 0.96), 7)
            buyablePrice = min(bestprice, boundryprice)

            # Check for buyable price
            print("Buyable Price: ", buyablePrice)
            if (buyablePrice < float(vlast)):
                return

            # Firebase true , BCHQuantity
            Fcheck, Fbal = self.checkfirebase(child)
            # print(Fcheck, Fbal)

            # Poloniex BCH Balance
            Pbal = float(polo().returnBalances("BTC"))
            Pbal = round(float(Pbal / vlast), 7)
            # print(Pbal)

            pushdata["rLast"] = vlast
            pushdata["rLow"] = vlow
            pushdata["rHigh"] = vhigh
            pushdata["rPbal"] = Pbal
            pushdata["rFbal"] = Fbal
            pushdata["rBuyablePrice"] = buyablePrice

            print("bestPrice:", bestprice, "boundryprice:", boundryprice, "buyableprice:", buyablePrice)

            pushdata["rException"] = -1  # default, change if not even Excpt not occur
            print("ToBuy")
            if (Fcheck and float(Fbal) > 0.0001 and float(Pbal) > 0.0001 and float(
                    Fbal) < Pbal and vlast < buyablePrice):
                print("Performing Trade")
                resultflag, resultData = polo().buyCoin(float(vlast), float(Fbal))
                pushdata["BuySuccess"] = resultflag
                if (resultflag == 1):
                    pushdata["TradeData"] = resultData
                    firebase().Setbuying(child, {"bIsbuying": 0})
                    self.updateSellingDB(child, vlast, Fbal)
                pushdata["rException"] = 0
                firebase().pushTransactionRecord(pushdata)
        except Exception as e:
            pushdata["rExceptionRes"] = str(e)
            pushdata["rException"] = 1
            print("Buying ExceptionOccured1")
            currException = firebase().Getbuying(child, "bExcptOccured")
            firebase().Setbuying(child, {"bExcptOccured": int(currException) + 1})
            firebase().pushTransactionRecord(pushdata)

    def tosell(self, child, factor):
        pushdata = {"rException": -1, "SellSuccess": 0, "Type": "ToSell", "Category": str(child) + str(factor)}
        resultflag = -1
        resultData = -1
        vlast = 0.0
        Fsellingprice = 0.0

        try:
            flagBuying = firebase().Getbuying(child, "bIsbuying")  # BuyingFlag
            userActive = firebase().Getselling(child, "sUseractive")
            if (flagBuying == 0 and userActive):
                vlast = float(polo().returnLastPrice("BTC_BCH")['last'])
                FsellingQuantity = float(firebase().Getselling(child, "BCHBalance"))
                Fsellingprice = float(firebase().Getselling(child, "SellingPrice"))

                print("ToSell = ", "last:", vlast, "Fselling:", Fsellingprice, "Amnt:", FsellingQuantity)
                if (float(vlast) > Fsellingprice and FsellingQuantity > 0.0001):  # >
                    # do a sell
                    resultflag, resultData = polo().sellCoin(vlast, FsellingQuantity)

                    pushdata["sQuantity"] = FsellingQuantity
                    pushdata["sPrice"] = vlast
                    pushdata["SellSuccess"] = resultflag

                    if (resultflag == 1):
                        firebase().Setbuying(child, {"bIsbuying": 1})  # BuyingFlag
                        pushdata["TradeData"] = resultData
                    pushdata["rException"] = 0
                    firebase().pushTransactionRecord(pushdata)
        except Exception as e:
            pushdata["rExceptionRes"] = str(e)
            pushdata["rException"] = 1
            print("Selling Exception")
            firebase().Setselling(child, {"sExcptOccured": 1})
            firebase().pushTransactionRecord(pushdata)
