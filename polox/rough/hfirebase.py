import pyrebase
import time
import datetime


class firebase:
    config = {
        "apiKey": "AIzaSyBIytNXAcKkxYUvM190b2iMPJwihZ4jEVI",
        "authDomain": "pythonpolo.firebaseapp.com",
        "databaseURL": "https://pythonpolo.firebaseio.com/",
        "storageBucket": "pythonpolo.appspot.com"
    }

    firebase = pyrebase.initialize_app(config)

    db = firebase.database()

    def Setbuying(self, child, data):
        x = self.db.child("buying").child(child).update(data)
        # print(x)
        return (x)

    def Getbuying(self, child, key):
        obj = self.db.child("buying").child(child).get().val()
        value = self.db.child("buying").child(child).get().val()[key]
        return (value)

    def Setselling(self, child, data):
        x = self.db.child("selling").child(child).update(data)
        # print(x)
        return (x)

    def Getselling(self, child, key):
        obj = self.db.child("selling").child(child).get().val()
        value = self.db.child("selling").child(child).get().val()[key]
        return (value)

    def pushTransactionRecord(self, data):
        data["timestamp"] = int(time.time())
        try:

            # data["date"]= str(datetime.utcnow().strftime('%d-%m-%Y %H:%M'))
            # self.db.child("Transactions").child("1").update( {"00" : 1000})

            counter = int(self.db.child("Transactions").child("1").get().val()["00"])
            self.db.child("Transactions").child("1").update({"00": counter - 1})
            self.db.child("Transactions").child("1").child(counter).update(data)
        except Exception as e:
            data["Exception_occured_In_Transaction "] = str(e)
            self.db.child("Transactions").child("Exception").update(data)

#--------------------------------------------------------------------------------------------


    def pushBCHdata(self,data):
        data["timestamp"] = int(time.time())
        try:
            self.db.child("BCHdata").child("1").child(int(time.time())).update(data)
        except Exception as e:
            print (e)

    def pushBCHMinPriceOccured(self,data):
        data["timestamp"] = int(time.time())
        try:
            # data["date"]= str(datetim     e.utcnow().strftime('%d-%m-%Y %H:%M'))
            self.db.child("BCHMinPrice").child("1").child(int(time.time())).update(data)
        except Exception as e:
            print (e)



    def setBuyfirebase(self, child):
        data = {}
        data["bUseractive"] = 99
        data["bIsbuying"] = 99
        data["bQuantity"] = 99
        data["bExcptOccured"] = 1
        user = self.Setbuying(child, data)

    def setSellfirebase(self, child):
        data = {}
        data["sUseractive"] = 99
        data["BCHBalance"] = 99
        data["SellingPrice"] = 99
        user = self.Setselling(child, data)
