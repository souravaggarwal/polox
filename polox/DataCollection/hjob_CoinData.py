import schedule
import time
from CoinDataPrep import preparingdata
import sys


def job():
    preparingdata().pushcoinData("BTC_BCH")
    preparingdata().pushcoinData("BTC_ETH")
    preparingdata().pushcoinData("BTC_LTC")
    preparingdata().pushcoinData("USDT_BTC")
    
    print("fineData")


schedule.every(0.1).minutes.do(job)
while True:
    schedule.run_pending()
    time.sleep(1)
