import schedule
import time
from hPoloniexRequest import polo
from hfirebase import firebase
import pandas as pd
#

class preparingdata:
    def currentdata(self, pbch, pbtc):
        pushdata = {}
        pushdata["BCHlast"] = pbch["last"]
        pushdata["BCHlow"] = pbch["low24hr"]
        pushdata["BCHhigh"] = pbch["high24hr"]
        pushdata["BCHpercent"] = pbch["percentChange"]
        pushdata["BTClast"] = pbtc["last"]
        pushdata["BTClow"] = pbtc["low24hr"]
        pushdata["BTChigh"] = pbtc["high24hr"]
        return (pushdata)

    def prices(self, data):
        # data=json.load(open('text.json'))
        ask = pd.DataFrame(data["asks"], columns=['a', 'b'])

        ask['c'] = pd.to_numeric(ask['a']) * pd.to_numeric(ask['b'])
        askPrice = ask['c'].sum() / pd.to_numeric(ask['b']).sum()

        buy = pd.DataFrame(data["bids"], columns=['a', 'b'])
        buy['c'] = pd.to_numeric(buy['a']) * pd.to_numeric(buy['b'])
        buyPrice = buy['c'].sum() / pd.to_numeric(buy['b']).sum()
        # print(buyPrice)
        # print(askPrice)
        return (buyPrice, askPrice)

    def BCHdata(self):
        pbch = polo().returnLastPrice("BTC_BCH")
        pbtc = polo().returnLastPrice("USDT_BTC")
        pushdata = self.currentdata(pbch, pbtc)
        bch = polo().returnOrderBook("BTC_BCH", 25)
        btc = polo().returnOrderBook("USDT_BCH", 25)
        bchtradebuyprice, bchtradesellprice = self.prices(bch)
        btctradebuyprice, btctradesellprice = self.prices(btc)

        pushdata["bchtradebuyprice"] = bchtradebuyprice
        pushdata["bchtradesellprice"] = bchtradesellprice
        pushdata["btctradebuyprice"] = btctradebuyprice
        pushdata["btctradesellprice"] = btctradesellprice
        firebase().pushBCHdata(pushdata)
        print("fine")


def job():
    preparingdata().BCHdata()



schedule.every(30).minutes.do(job)
while True:
    schedule.run_pending()
    time.sleep(1)
