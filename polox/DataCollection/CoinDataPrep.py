import schedule
import time
from hPoloniexRequest import polo
from hfirebase import firebase
import pandas as pd
import sys


class preparingdata:
    externalcoin =0
    def currentdata(self, coin):
        pushdata = {}
        pushdata["last"] = coin["last"]
        pushdata["low24"] = coin["low24hr"]
        pushdata["high24"] = coin["high24hr"]
        pushdata["percent"] = coin["percentChange"]
        pushdata["lowestAsk"] = coin["lowestAsk"]
        pushdata["highestBid"] = coin["highestBid"]
        pushdata["baseVol"] = coin["baseVolume"]
        pushdata["quoteVol"] = coin["quoteVolume"]
        return (pushdata)

    def OT_DataCalc(self, data,count):
        try:
            # data=json.load(open('text.json'))
            ask = pd.DataFrame( data["asks"][0:count], columns =['a','b'] )
            ask['c'] = pd.to_numeric(ask['a']) * pd.to_numeric(ask['b'])
            askPrice = ask['c'].sum() / pd.to_numeric(ask['b']).sum()
            buy = pd.DataFrame(data["bids"][0:count], columns=['a', 'b'])
            buy['c'] = pd.to_numeric(buy['a']) * pd.to_numeric(buy['b'])
            buyPrice = buy['c'].sum() / pd.to_numeric(buy['b']).sum()
            #print({  'buy' : buyPrice  , 'Ask' : askPrice } )
            return (str(buyPrice), str(askPrice))
        except:
            print("Exception")


    def orderTradeData(self,OTdata,askorsell,count):
        try:
            data=pd.DataFrame(OTdata[askorsell] ,columns=['price','quant'])
            sums=0
            i=0
            for i in range(0,1190):
                i=i+1

                sums = sums + data['quant'][i]

                if ( sums >= count):
                    datas = (data['price'][0:i]).astype('float')
                    price = ( datas.sum() ) / (len( datas ))
                    #print( { 'Price' : price , 'Length' : len(datas) } )
                    return(str(price))
        except:
            print(sys.exc_info())
            print(self.externalcoin,count,sums,i,datas ,str(datas.sum()) , str(len(datas)) )


    def pushcoinData(self , coin  ):
        pushdata={}
        if(coin != "USDT_BTC"):
            self.externalcoin =coin
            lastData = polo().returnLastPrice(coin)
            pushdata = self.currentdata(lastData)
            OTdata = polo().returnOrderBook(coin,1200)

            pushdata["OT_Buy25"] , pushdata["OT_Ask25"] = self.OT_DataCalc(OTdata,25)
            pushdata["OT_Buy50"] , pushdata["OT_Ask50"] = self.OT_DataCalc(OTdata,50)
            pushdata["OT_Buy75"] , pushdata["OT_Ask75"] = self.OT_DataCalc(OTdata,75)
            pushdata["OT_Buy100"] , pushdata["OT_Ask100"] = self.OT_DataCalc(OTdata,100)
            pushdata["OT_Buy150"] , pushdata["OT_Ask150"] = self.OT_DataCalc(OTdata,150)
            pushdata["OT_Buy200"] , pushdata["OT_Ask200"] = self.OT_DataCalc(OTdata,200)
            pushdata['OTAsk_Quant100'] =self.orderTradeData(OTdata, "asks", 100)
            pushdata['OTBid_Quant100'] =self.orderTradeData(OTdata, "bids", 100)
            pushdata['OTAsk_Quant250'] =self.orderTradeData(OTdata, "asks", 250)
            pushdata['OTBid_Quant250'] =self.orderTradeData(OTdata, "bids", 250)
            pushdata['OTAsk_Quant500'] =self.orderTradeData(OTdata, "asks", 500)
            pushdata['OTBid_Quant500'] =self.orderTradeData(OTdata, "bids", 500)
            pushdata['OTAsk_Quant900'] =self.orderTradeData(OTdata, "asks", 900)
            pushdata['OTBid_Quant900'] =self.orderTradeData(OTdata, "bids", 900)
            #print(pushdata)

            # USDT + Coin
            USDT_coin = coin
            USDT_coin = USDT_coin.replace("BTC","USDT")
            USDT_data = polo().returnLastPrice(USDT_coin)
            pushdata["USDTlast"] = USDT_data["last"]
            pushdata["USDTlow"] = USDT_data["low24hr"]
            pushdata["USDThigh"] = USDT_data["high24hr"]

            # BTC
            BTC_data = polo().returnLastPrice("USDT_BTC")
            pushdata["BTClast"] = BTC_data["last"]
            pushdata["BTClow"] = BTC_data["low24hr"]
            pushdata["BTChigh"] = BTC_data["high24hr"]


        else:
            self.externalcoin =coin
            lastData = polo().returnLastPrice(coin)
            pushdata = self.currentdata(lastData)
            OTdata = polo().returnOrderBook(coin,1200)

            pushdata["OT_Buy25"] , pushdata["OT_Ask25"] = self.OT_DataCalc(OTdata,25)
            pushdata["OT_Buy50"] , pushdata["OT_Ask50"] = self.OT_DataCalc(OTdata,50)
            pushdata["OT_Buy75"] , pushdata["OT_Ask75"] = self.OT_DataCalc(OTdata,75)
            pushdata["OT_Buy100"] , pushdata["OT_Ask100"] = self.OT_DataCalc(OTdata,100)
            pushdata["OT_Buy150"] , pushdata["OT_Ask150"] = self.OT_DataCalc(OTdata,150)
            pushdata["OT_Buy200"] , pushdata["OT_Ask200"] = self.OT_DataCalc(OTdata,200)
            pushdata['OTAsk_Quant100'] =self.orderTradeData(OTdata, "asks", 5)
            pushdata['OTBid_Quant100'] =self.orderTradeData(OTdata, "bids", 5)
            pushdata['OTAsk_Quant250'] =self.orderTradeData(OTdata, "asks", 10)
            pushdata['OTBid_Quant250'] =self.orderTradeData(OTdata, "bids", 10)
            pushdata['OTAsk_Quant500'] =self.orderTradeData(OTdata, "asks", 20)
            pushdata['OTBid_Quant500'] =self.orderTradeData(OTdata, "bids", 20)
            pushdata['OTAsk_Quant900'] =self.orderTradeData(OTdata, "asks", 4)
            pushdata['OTBid_Quant900'] =self.orderTradeData(OTdata, "bids", 4)



        firebase().pushCoinData( pushdata, coin)
        #except:
        #    print("Exception")




#def job():
#preparingdata().BCHdata()



#schedule.every(30).minutes.do(job)
#while True:
#    schedule.run_pending()
#    time.sleep(1)
