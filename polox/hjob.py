import schedule
import time
from hPoloniexRequest import polo
from hfirebase import firebase
from hbuysell import data

#

class preparingdata:
    def currentdata(self, pbch, pbtc):
        pushdata = {}
        pushdata["BCHlast"] = pbch["last"]
        pushdata["BCHlow"] = pbch["low24hr"]
        pushdata["BCHhigh"] = pbch["high24hr"]
        pushdata["BCHpercent"] = pbch["percentChange"]
        pushdata["BTClast"] = pbtc["last"]
        pushdata["BTClow"] = pbtc["low24hr"]
        pushdata["BTChigh"] = pbtc["high24hr"]
        return (pushdata)


def job():
    pTic = polo().returnLastPrice("BTC_BCH")
    vlast = float(pTic['last'])
    vhigh = float(pTic['high24hr'])
    vlow = float(pTic['low24hr'])
    xfac = (vhigh - vlow)

    b = 7
    a = 7
    per=(((vhigh-vlow) / vlow) *1/10)
    print( "Buy",(vlow + (vlow*per)) , vlast)
    if( (vlow + (vlow*per)) > vlast):
    	if (vlow + (xfac / b) > vlast):
    		data().tobuy("b", b)

    if (round(float(pTic["last"]), 3) <= round(float(pTic["low24hr"]), 3)):
        if (vlow + (xfac / a) > vlast):
            data().tobuy("a", a)

    data().tosell("a", a)
    data().tosell("b", b)

    #-------------------------------------------
    pbch =pTic
    pbtc = polo().returnLastPrice("USDT_BTC")

    if (round(float(pbch["last"]), 3) <= round(float(pbch["low24hr"]), 3)):
        pushdata = preparingdata().currentdata(pbch, pbtc)
        firebase().pushBCHMinPriceOccured(pushdata)


schedule.every(1).minutes.do(job)
while True:
    schedule.run_pending()
    time.sleep(1)
